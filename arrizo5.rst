艾瑞泽5碰撞集锦
==================================

.. note::

        截至2016年3月23日共收录2起艾瑞泽5事故。原则上尽量只收录能够反应车辆安全性的、较为大型的碰撞，小刮擦和小追尾，只能反应车辆前后包围件的硬度，不能真正反应车辆安全性

[小偏置碰撞] 试驾车紧急避让三轮车，最终偏置撞上杆。 `事故原委与图片 <http://club.autohome.com.cn/bbs/threadowner-c-3405-50781364-1.html>`_

.. image:: /_static/037.jpg
        :align: center

[追尾] 试驾车被本田追尾，本田损失大很多。 `事故图片 <http://mp.weixin.qq.com/s?__biz=MjM5Nzg3Mjg2Mg==&mid=405670179&idx=7&sn=15e264c94847abad31a3c4f78fa7cfaa&scene=0#wechat_redirect>`_

.. image:: /_static/038.jpg
        :align: center


