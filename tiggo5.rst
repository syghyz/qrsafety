瑞虎5碰撞集锦
=========================

疑似追尾大货车。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-3195-45078599-1.html>`_

.. image:: /_static/042.jpg
        :align: center

宝马5系追尾瑞虎5，宝马完败。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-3195-43197885-1.html>`_

.. image:: /_static/043.jpg
        :align: center

右后侧撞击事故车。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-41141597-1.html>`_

.. image:: /_static/044.jpg
        :align: center

大货车追尾被顶入前方大货车，瑞虎5保护车主安全。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-48024539-1.html>`_

.. image:: /_static/045.jpg
        :align: center

大货夹击铁圈压顶，瑞虎5表现出色。 `图片 <http://www.cherybbs.com/forum.php?mod=viewthread&tid=7419&fromuid=5887>`_

.. image:: /_static/052.jpg
        :align: center

翻滚事故车，变形不大。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-38781034-1.html>`_

.. image:: /_static/046.jpg
        :align: center

冲入汽车装潢店。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-28857003-1.html>`_

.. image:: /_static/047.jpg
        :align: center

瑞虎5高速vs大众polo。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-33744062-1.html>`_

.. image:: /_static/048.jpg
        :align: center

捷达猛烈撞击瑞虎5，捷达报废，瑞虎5维修。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-40732532-1.html>`_

.. image:: /_static/049.jpg
        :align: center

疑似右侧偏置撞击大货车，受损严重。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-37237601-1.html>`_

.. image:: /_static/050.jpg
        :align: center

碰撞大货车，乘员没受伤。 `图片 <http://club.autohome.com.cn/bbs/thread-c-3195-33275271-1.html>`_

.. image:: /_static/051.jpg
        :align: center
