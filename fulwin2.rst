风云2碰撞集锦
=========================

[正面]高速车祸，切身感受风云2的安全性。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-13990394-1.html>`_

.. image:: /_static/053.jpg
        :align: center

[被压]托头货车压顶风云2，车主奇迹般无碍。 `链接 <http://club.autohome.com.cn/bbs/thread-c-2980-45043314-1.html>`_

.. image:: /_static/054.jpg
        :align: center

[前后夹击]被搅拌车追尾、追尾卡车，受损不大自己开到维修部。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-36014127-1.html>`_

.. image:: /_static/055.jpg
        :align: center

[前后夹击]被货车前后夹击，风云2、A3现奇迹。 `图片 <http://club.autohome.com.cn/bbs/thread-c-797-18335168-1.html>`_

.. image:: /_static/056.jpg
        :align: center

[前后夹击]追尾SUV又被雷克萨斯SUV追尾。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-13200009-1.html>`_

.. image:: /_static/057.jpg
        :align: center

[被追尾]被大卡车以70-80速度追尾。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-9489044-1.html>`_

.. image:: /_static/057.jpg
        :align: center

[被追尾]风云2被箱货追尾。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-14733935-1.html>`_

.. image:: /_static/058.jpg
        :align: center

[滚下山坡]30米山坡滚下，F2基本报废，人员无事。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-14233584-1.html>`_

.. image:: /_static/059.jpg
        :align: center

[撞树]撞到摩托车再连撞3棵小树，风云2车内人员无大碍。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-21594542-1.html>`_

.. image:: /_static/060.jpg
        :align: center

[正面] 风云2正面撞翻小货车。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-14225928-1.html>`_

.. image:: /_static/061.jpg
        :align: center

[撞杆]风云2雪地撞到水泥杆，受损不大。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-37403338-1.html>`_

.. image:: /_static/062.jpg
        :align: center

[撞杆] F22 PK 电线杆 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-13930300-1.html>`_

.. image:: /_static/063.jpg
        :align: center

[正面]雨天借车开撞墙，风云2表现不错。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-18633091-1.html>`_

.. image:: /_static/064.jpg
        :align: center

[侧后撞]悦翔侧后撞风云2，风云2完胜。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-27275300-1.html>`_

.. image:: /_static/065.jpg
        :align: center

[撞杆]风云2疑似侧面撞杆（根据受损图判断），右侧凹进，车主无大碍。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-18304014-1.html>`_

.. image:: /_static/066.jpg
        :align: center

[小偏置]君威小偏置碰撞风云2，风云2乘客轻伤。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-28879443-1.html>`_

.. image:: /_static/067.jpg
        :align: center

[翻滚]风云2雨天撞山翻车，司机逃生。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-24007773-1.html>`_

.. image:: /_static/068.jpg
        :align: center

[侧面]风云2遭侧面撞击，司机无碍。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-23215505-1.html>`_

.. image:: /_static/069.jpg
        :align: center

[钻底]风云2钻底大卡车，人员仅受轻伤。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-22260050-1.html>`_

.. image:: /_static/070.jpg
        :align: center

[翻车]风云2翻车。 `图片 <http://club.autohome.com.cn/bbs/thread-c-837-5539640-1.html>`_

.. image:: /_static/071.jpg
        :align: center

滚下路基，二人轻伤，图片已删除。 `链接 <http://club.autohome.com.cn/bbs/thread-c-837-13931739-1.html>`_
