艾瑞泽7碰撞集锦
======================

.. note::

        截至2016年3月23日共收录36起事故。原则上尽量只收录能够反应车辆安全性的、较为大型的碰撞，小刮擦和小追尾，只能反应车辆前后包围件的硬度，不能真正反应车辆安全性。

偏置类碰撞
------------------

[小偏置碰撞]洛阳滨河北路艾瑞泽7小偏置碰撞，车速约89，A柱基本完好，驾驶舱保持完好。 `事故图片 <http://bbs.ly.shangdu.com/read.php?tid=4202124&uid=79947&displayMode=1>`_ `现场情况 <http://club.autohome.com.cn/bbs/threadowner-c-2980-24518157-1.html#pvareaid=101435>`_ `车速揭秘 <http://club.autohome.com.cn/bbs/threadowner-c-2980-28966041-1.html#pvareaid=101435>`_ `同一路段宝马5系事故对比(A柱切割) <http://bbs.ly.shangdu.com/read.php?tid=4233780&uid=79947&displayMode=1>`_ `同一路段奔腾X80事故对比(有死亡) <http://bbs.ly.shangdu.com/read.php?tid=4522364&uid=478692&displayMode=1>`_

.. image:: /_static/001.jpg
        :align: center

[小偏置碰撞]艾瑞泽7碰撞水泥墩。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-25186274-1.html#pvareaid=101435>`_

.. image:: /_static/002.jpg
        :align: center

[偏置碰撞]艾瑞泽7偏置碰撞泥头车，车速超60，成功保卫乘客。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-36471871-1.html#pvareaid=101435>`_ `事故视频 <http://v.youku.com/v_show/id_XODQ0NTU4NjI4.html>`_

.. image:: /_static/003.jpg
        :align: center

[偏置碰撞]艾瑞泽7偏置碰撞皮卡。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-30309658-1.html#pvareaid=101435>`_

.. image:: /_static/004.jpg
        :align: center

[偏置碰撞]艾瑞泽7偏置碰撞帕萨特。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-42003827-1.html#pvareaid=101435>`_

.. image:: /_static/005.jpg
        :align: center

[偏置碰撞]艾瑞泽7偏置碰撞槽罐车，驾驶舱岿然不动。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45493994-1.html#pvareaid=101435>`_

.. image:: /_static/006.jpg
        :align: center

[偏置碰撞]艾瑞泽7与宝马偏置碰撞，双方损失差不多。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-42730299-1.html#pvareaid=101435>`_

.. image:: /_static/007.jpg
        :align: center

正面类碰撞
-----------------------

[高速]艾瑞泽7高速120km/h碰撞，乘员没有受伤。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-2980-29920067-1.html>`_

.. image:: /_static/008.jpg
        :align: center

[追尾]艾瑞泽7撞扁朗逸，朗逸三厢变两厢，艾瑞泽7变形很小。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-26532501-1.html#pvareaid=101435>`_

.. image:: /_static/009.jpg
        :align: center

[追尾]艾瑞泽7以60-70km时速追尾途观。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-42521115-1.html#pvareaid=101435>`_

.. image:: /_static/010.jpg
        :align: center

[追尾]把普桑撞成两厢，艾瑞泽7变形不大。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-43993279-1.html#pvareaid=101435>`_

.. image:: /_static/011.jpg
        :align: center

[连环追尾]高速连环追尾，斯巴鲁压上艾瑞泽7。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-33990988-1.html#pvareaid=101435>`_

.. image:: /_static/012.jpg
        :align: center

[连环追尾]艾瑞泽7疑似追尾大货并遭前后夹击，发动机舱严重变形，A柱依然完好。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45354438-1.html#pvareaid=101435>`_

.. image:: /_static/013.jpg
        :align: center

[连环追尾]四车连环追尾，艾瑞泽7表现不错。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45940902-1.html#pvareaid=101435>`_

.. image:: /_static/014.jpg
        :align: center

[连环追尾]沪渝高速连环追尾。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-38906185-1.html#pvareaid=101435>`_

.. image:: /_static/016.jpg
        :align: center

[被追尾]艾瑞泽7被重型集装箱追尾，驾驶舱基本保持完好。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-27852103-1.html#pvareaid=101435>`_

.. image:: /_static/017.jpg
        :align: center

[被追尾]高速堵车被皮卡追尾，车速不太快，损失非常小。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45823547-1.html#pvareaid=101435>`_

.. image:: /_static/018.jpg
        :align: center

[钻底]酒驾钻底大货车，A柱铁骨铮铮，艾瑞泽7临危救主。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-32420559-1.html#pvareaid=101435>`_

.. image:: /_static/019.jpg
        :align: center

[钻底]艾瑞泽7钻底沙土车，仅受轻伤。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-3397-32069631-1.html#pvareaid=101435>`_

.. image:: /_static/020.jpg
        :align: center

[钻底]晋江艾瑞泽7钻底沙土车，A柱铁骨铮铮，艾瑞泽7成功救主。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-43077721-1.html#pvareaid=101435>`_

.. image:: /_static/021.jpg
        :align: center

[对撞]艾瑞泽7撞翻皮卡，驾驶舱没变形。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45239723-1.html#pvareaid=101435>`_

.. image:: /_static/022.jpg
        :align: center

[撞杆撞树]艾瑞泽7撞杆，70-80速度，成功护主。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-34604560-1.html#pvareaid=101435>`_

.. image:: /_static/023.jpg
        :align: center

[撞杆撞树]80-90km/h暴力撞树之后又弹入沟，乘员仅受轻伤。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-35392092-1.html#pvareaid=101435>`_

.. image:: /_static/024.jpg
        :align: center

[撞山]艾瑞泽7酒驾撞山。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-34167075-1.html#pvareaid=101435>`_

.. image:: /_static/025.jpg
        :align: center

[撞翻其他车]艾瑞泽7撞翻比亚迪SUV。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-2980-48468849-1.html>`_

.. image:: /_static/026.jpg
        :align: center

[撞路左右侧]雨天90-100时速开车，撞上道路左右侧。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-3405-50604466-9.html>`_ （第167楼）

.. image:: /_static/036.jpg
        :align: center


侧面类碰撞
-------------------

[侧面]卡车撞艾瑞泽7侧面。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-36508019-1.html#pvareaid=101435>`_

.. image:: /_static/027.jpg
        :align: center

[侧面]福特侧撞艾瑞泽7，艾瑞泽7表现优秀。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-45471982-1.html#pvareaid=101435>`_

.. image:: /_static/028.jpg
        :align: center

[侧面]宝马X5台州碰撞艾瑞泽7，艾瑞泽7胜出。 `图片1 <http://club.autohome.com.cn/bbs/thread-c-2980-47593147-1.html>`_ `图片2 <http://taizhou.19lou.com/forum-830-thread-141991449015746912-1-1.html>`_ `图片3 <http://taizhou.19lou.com/forum-827-thread-163531449012120726-1-1.html?qq-pf-to=pcqq.group>`_

.. image:: /_static/029.jpg
        :align: center

[高速侧面]高速侧面被撞击。 `图片 <http://club.autohome.com.cn/bbs/thread-c-2980-45869843-1.html>`_ 

.. image:: /_static/015.jpg
        :align: center

其他类事故
-------------------

[翻下悬崖]艾瑞泽7翻下悬崖坠河，驾驶舱基本保持完好。 原帖 `#1 <http://club.autohome.com.cn/bbs/threadowner-c-2980-30673835-1.html#pvareaid=101435>`_ `#2 <http://club.autohome.com.cn/bbs/threadowner-c-2980-30673835-1.html#pvareaid=101435>`_ `#3 <http://club.autohome.com.cn/bbs/threadowner-c-2980-30674502-1.html#pvareaid=101435>`_ （原帖图片被删除） `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-30814859-1.html#pvareaid=101435>`_

.. image:: /_static/030.jpg
        :align: center

[失控入渠]100km/h入弯，陷水沟酿大货。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-29340571-1.html#pvareaid=101435>`_

.. image:: /_static/031.jpg
        :align: center

[翻车]超速撞石演翻车，艾瑞泽7变形小。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-35896115-1.html#pvareaid=101435>`_

.. image:: /_static/032.jpg
        :align: center

[撞遗撒]卡车遗撒，艾瑞泽7躲避不及。 `事故图片 <http://club.autohome.com.cn/bbs/threadowner-c-2980-29537148-1.html#pvareaid=101435>`_

.. image:: /_static/033.jpg
        :align: center

[侧后方]三菱正面碰撞艾瑞泽7侧后方。 `事故图片 <http://club.autohome.com.cn/bbs/thread-c-2980-38629998-1.html>`_

.. image:: /_static/034.jpg
        :align: center

[翻滚]艾瑞泽7翻滚4圈无大碍。 `事故图片 <http://tieba.baidu.com/p/4250993245>`_

.. image:: /_static/035.jpg
        :align: center
